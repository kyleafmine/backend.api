module.exports = require('rethinkdbdash')({
    port: 28015,
    host: 'localhost',
    db: 'fanderov_games',
    silent: true,
    discovery: true,
});