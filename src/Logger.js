const chalk = require('chalk');
const dateformat = require('dateformat');

class Logger {
    static getDateTimeMarker() {
        return dateformat(new Date, 'mm/dd/yyyy | HH:MM:ss:L', true)
    }

    static info(...messages) {
        console.log(chalk`{bold.bgWhite.grey ${this.getDateTimeMarker()}} {bold.cyan [INFO]} - ${messages.join(' ')}`);
    }
    
    static warn(...messages) {
        console.log(chalk`{bold.bgWhite.grey ${this.getDateTimeMarker()}} {bold.yellow [WARNING]} - ${messages.join(' ')}`);
    }
    
    static error(...messages) {
        console.log(chalk`{bold.bgWhite.grey ${this.getDateTimeMarker()}} {bold.red [ERROR]} - ${messages.join(' ')}`);
    }
    
    static debug(...messages) {
        console.log(chalk`{bold.bgWhite.grey ${this.getDateTimeMarker()}} {bold.blue [DEBUG]} - ${messages.join(' ')}`);
    }
    
    static event(eventTag, ...messages) {
        console.log(chalk`{bold.bgWhite.grey ${this.getDateTimeMarker()}} {bold.magenta [EVENT]} {bold.hex('#FF8800') ${eventTag}} - ${messages.join(' ')}`);
    }
    static chat(...messages) {
        console.log(chalk`{bold.bgWhite.grey ${this.getDateTimeMarker()}} ${messages.join(" ")}` )
    }
}

module.exports = Logger;
