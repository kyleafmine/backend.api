const db = require('../db')
const flake = require('simpleflake')
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
class Author {
    constructor() {
        this.name = "Unknown"
        this.token = "Unknown"
        this.id = "Unknown"
    }
    /**
     * Authenticates a token against the auth servers
     * @param {string} bearer - bearer token for user retrieval 
     * @return {Promise} User
     */
    bearer_auth(bearer) {
        var n = this
        return new Promise((resolve, reject) => {
            db.table('users').filter({
                bearer: bearer
            }).run((e, result) => {
                  if (e) throw e;
                  if (isEmpty(result)) {
                      reject({error: "invalid bearer token"})
                  } else {
                      var user = result[0]
                      n.name = user.username
                      n.id = user.userid
                      n.token = bearer
                      n.db = user
                      resolve(n)
                  }
              });
        })

    }

    /**
     * Authenticates a user for debug purposes
     * @param {string} name - Name for mock user
     * @param {string} bearer - Mock bearer token
     * @returns {object} this
     */
    debug_auth(name, bearer) {
        this.name = name
        this.token = bearer
        this.id = require('crypto').createHash('md5').update(bearer).digest("hex")
        this.permissionlevel = 'ADMIN'
        return this
    }
    /**
     * Converts this object into JSON suitible for HTTP
     * @returns {object}
     */
    json() {
        return {
            name: this.name,
            id: this.id
        }
    }
    toString() {
        return `${this.name}:${this.id}:${this.permissionlevel}`
    }
}
module.exports = Author