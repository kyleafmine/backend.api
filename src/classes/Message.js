/**
 * A number, or a string containing a number.
 * @typedef {object} AuthorResolvable - A User object or a UID string
*/
const flake = require('simpleflake');
const User = require('./Author');
const markdown = require('markdown').markdown;
class Message {
    /** 
    * Constructs message
    * @param {string} content - Message content (Steralized)
    * @param {AuthorResolvable} author - UID of message author
    * @param {string} room - Tag of the room message is sent in
    * @param {boolean} [server=false] - If the author of the message is the server
    * @param {boolean} [disablemarkdown=false] - Disable markdown parsing
    * @returns this
    */
    constructor(content, author, room, server = false, disablemarkdown = false) {
        this.content = content || "invalid message";
        if (!disablemarkdown) {
            this.content = markdown.toHTML(content).replace(/^<p>/g, "").replace(/<\/p>$/g, "");
        }
        this.author = author || {e: "invalid author"};
        this.server = server,
        this.room = room || "invalid room",
        this.id = flake(new Date()).toString('base10');
        if (server) {
            this.author = {
                name: "SERVER",
                id: "0"
            };
        }
    }
    
    /**
     * Sends message
     * @param {object} io - Socket.IO server object
     * @param {boolean} [broadcast=false] - Broadcasts the message to all rooms
     */
    send(io, broadcast = false, direct = false) {
        if (broadcast || direct) {
            io.emit("message-create", {
                content: this.content,
                author: this.author,
                server: this.server,
                id: this.id
            });

        } else {
            io.to(this.room).emit("message-create", {
                content: this.content,
                author: this.author,
                server: this.server,
                id: this.id
        });
      }
    }
    /**
     * Returns a JSON Object sutible for storage
     * @returns {object} Message
     */
    toJSON() {
        return {
            content: this.content,
            author: this.author,
            server: this.server,
            room: this.room,
            id: this.id
        };
    }
    static from(json){
        return Object.assign(new Message(), json);
    }
}

module.exports = Message;