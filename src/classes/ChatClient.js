// Thanks to someone on stackoverflow for this
/*eslint-disable no-extra-parens */
function guid() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

/**
 * A number, or a string containing a number.
 * @typedef {object} AuthorResolvable - A User object or a UID string
*/

class Message {
    /** 
    * Constructs message
    * @param {string} content - Message content 
    * @param {string} bearer - Bearer token for authentication                                                                                        
    * @param {string} room - Name of room to send message to
    */
    constructor(content, bearer, room) {
        //if (!content || !bearer || !room) throw new TypeError("Invalid construct")
        this.content = content;
        this.bearer = bearer; 
        this.room = room; 
        
    }

    /**
     * Returns a JSON Object sutible for storage
     * @returns {object} Message
     */
    toJSON() {
        return {
            content: this.content,
            bearer: this.bearer,
            room: this.room
        };
    }
    static from(json){
        return Object.assign(new Message(), json);
    }
}


class ChatClient extends EventEmitter {
    /**
     * @param {string} bearer - Bearer token, required()
     * @param {string} [url] - Optional url
     */
    constructor(bearer, url) {
        if (!bearer) throw new TypeError("Requires bearer token");
        super();
        this.socket = io(url || undefined);
        this.nickname = "Anon";
        this.room = "global";
        this.bearer = bearer;
        this.socket.on('message-create', (msg) => {
            var n = Message.from(msg);
            this.emit("new-message", n);
        });
        this.socket.on('nick-change', (data) => {
            this.emit('nick-change', data);
        });
        this.socket.on('auth-confirm', id => {
            this.id = id;
            this.emit("auth", id)
        });
        this.socket.on('room-switch', data => {
            this.room = data.new;
            this.emit('room-switch', data);
        });
        console.log("join");
        this.socket.emit("join", {
            bearer: this.bearer
        });
        this.socket.on("invalid-bearer", () => {
            alert("invalid login, please retry")
            window.location.pathname = "/auth/logout"
        })
    }
    /**
     * @param {Message} message - Message object
     * @returns {object} this
     */
    send(message) {
        if (!message instanceof Message) throw new TypeError("Supply message class!");
        this.socket.emit("message-send", message.toJSON());
    }
    /**
     * 
     * @param {string} nickname 
     * @returns {object} this
     */
    setNick(nickname) {
        //todo
    }
    switchroom(newroom) {
        this.socket.emit('room-switch-request', {
            new: newroom,
            old: this.room,
            bearer: this.bearer
        });        
    }
    disconnect() {
        this.socket.emit("chat_disconnect", {
            bearer: this.bearer
        })
    }
    

}