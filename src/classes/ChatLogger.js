const chalk = require('chalk');
const dateformat = require('dateformat');
class Logger {
    constructor() {
        
    }
    date() {
        return dateformat(new Date(), 'mm/dd/yyyy | HH:MM:ss:L', true);
    }
    info(...messages) {
        var msg = messages.join(" ");
        var x = chalk`{bold.grey.bgWhite ${this.date()}} {bold.cyan [INFO]} {bold.grey ${msg}} `;
        console.log(x);
    }
    chat(...messages) {
        var msg = messages.join(" ");
        var x = chalk`{bold.grey.bgWhite ${this.date()}} {bold.magenta [CHAT]} {bold.grey ${msg}} `;
        console.log(x);
    }
    warn(...messages) {
        var msg = messages.join(" ");
        var x = chalk`{bold.grey.bgWhite ${this.date()}} {yellow [WARN] ${msg}} `;
        console.log(x);
    }
    error(...messages) {
        var msg = messages.join(" ");
        var x = chalk`{bold.grey.bgWhite ${this.date()}} {red [ERROR] ${msg}} `;
        console.log(x);
    }
    custom(...messages) {
        var msg = messages.join(" ");
        var x = chalk`{bold.grey.bgWhite ${this.date()}} {grey.bold ${msg}}`;
        console.log(x);
    }
    user(user, socket) {
        if (socket) {
            return chalk`[{bold.green ${socket.handshake.address.match(/[0-9]{1,3}.*/g)}}:{bold.yellow ${user.id}}:{bold.magenta ${user.nickname || user.name}}]`;
        } else {
            return chalk`[{bold.yellow ${user.id}}:{bold.magenta ${user.nickname || user.name}}]`;
        }
    }
    user_message(user, message, socket) {
        return chalk`[{bold.green ${socket.handshake.address.match(/[0-9]{1,3}.*/g)[0]}}:{bold.yellow ${user.id}}:{bold.magenta ${user.nickname || user.name}}:{bold.cyan ${message.room}}]`;
    }
    server_message(room) {
        return chalk`[{bold.green *:2800}:{bold.yellow 0}:{bold.magenta SERVER}:{bold.cyan ${room}}]`;
    
        
    }
}


module.exports = new Logger();