/*eslint-disable unknown-require */
const Message = require('./Message');
const User = require('../classes/Author');
const logger = require('./ChatLogger');
const chalk = require('chalk');
const Filter = require('bad-words')
var filter = new Filter()
const Command = require('./Command')
class ChatIOServer {
    /**
     * 
     * @param {object} io - "io server" 
     * @returns this
     */
    constructor(io) {
        this.commands = new Map()
        io.on("connection", (socket) => {
            logger.custom(chalk`{bold.cyan [SOCKET-CONNECT]} {bold.grey [{bold.green ${socket.handshake.address.match(/[0-9]{1,3}.*/g)[0]}}]}`);
            socket.join("global");
            socket.on("message-send", async (data) => {
                var user = await new User().bearer_auth(data.bearer);
                
                
                var m = new Message(data.content, user.json(), data.room);
                if (data.content.startsWith("/")) {
                    if (this.commands.has(data.content.match(/\/\w*/g)[0].replace("/", ""))) {
                       return this.commands.get(data.content.match(/\/\w*/g)[0].replace("/", "")).run(user, data.room, data.content, "globalChannel", socket)                }
            }

                logger.chat(logger.user_message(user, m, socket), chalk`{white ${data.content}}`);
                var message = new Message(filter.clean(data.content), user.json(), data.room, false);
                message.send(io);
            });;
            socket.on("join", async (data) => {
                var author = false
                try {
                    author = await new User().bearer_auth(data.bearer);
                } catch {
                    socket.emit("invalid-bearer")
                    logger.custom(chalk`{bold.red [USER-AUTH-FAIL]} {bold.grey ${data.bearer}}`);
                }

                var message = new Message("[CONNECT] " + author.name || "Unknown", "server", "global", true);
                message.send(io, true);
                socket.emit("auth-confirm", author.id);
                logger.custom(chalk`{bold.cyan [USER-AUTH]} {bold.grey ${logger.user(author, socket)}}`);
            });
            socket.on("room-switch-request", (data) => {
                var author = new User().bearer_auth(data.bearer); 
                socket.leave(data.old);
                socket.join(data.new);
                socket.emit("room-switch", data);
                logger.custom(chalk`{bold.cyan [SWITCH-ROOM]} ${logger.user_message(author, {room: data.old}, socket)} {bold.italic.green =>} [:{cyan.bold ${data.new}}]`);
                new Message("[ROOM-LEAVE] " + author.name, "server", data.old, true).send(io);
                new Message("[ROOM-JOIN] " + author.name, "server", data.new, true).send(io);
                logger.chat(chalk`${logger.server_message(data.old)} {white.bold [ROOM-LEAVE] ${author.name}}`)
                logger.chat(chalk`${logger.server_message(data.new)} {white.bold [ROOM-JOIN] ${author.name}}`)
                
            });
            socket.on('chat_disconnect', async (data) => {
                console.log("start disconnect")
                console.log(data)
                var author = await new User().bearer_auth(data.bearer)
                new Message("[DISCONNECT] " + author.name, "server", "any", true).send(io, true)
                logger.custom(chalk`{bold.cyan [USER-DISCONNECT]} {bold.grey ${logger.user(author, socket)}}`);
            })
        });
        this.io = io;

    }
    register_command(command) {
        this.commands.set(command.command, command)
    }
}
module.exports = ChatIOServer;