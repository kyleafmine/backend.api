const User = require('./Author')
class Command {
    constructor(options) {
        this.context = options.context;
        this.command = options.command;
        this.execute = options.execute;
        this.permissionlevel = options.permissionlevel;
    }
    /**
     * Runs command handeler
     * @param {User} user - User object of command executor
     * @param {string} channel - Channel where command was executed
     * @param {ChannelContext} [context=publicChannel] - Context of command (public channel, private channel, guild channel...)
     * @param {object} io - Socket.IO server
     * @returns Promise
     */
    run(user, channel, message, context, io) {
        if (typeof this.context == "string") {
            if (this.context != context) return {error: "COMMAND_WRONG_CONTEXT"}
        }
        if (typeof this.context == "object") {
            if (!this.context.includes(context)) return {error: "COMMAND_WRONG_CONTEXT"}
            
        }
        return new Promise((resolve, reject) => {
            let result
            try {
                result = this.execute(user, channel, message, context, io);
            } catch(e) {
                reject(e)
            }
            resolve(result)
        })

    }
}
module.exports = Command