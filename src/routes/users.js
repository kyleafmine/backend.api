const express = require('express');
const flake = require('simpleflake')
const util = require('util');

const db = require('../db');
const Logger = require('../Logger');

Array.prototype.contains = function(element){
    return this.indexOf(element) > -1;
};

const usersRouter = express.Router();

usersRouter.get('/', (req,res) => {
    res.json({'why':'hello there!'})
    Logger.info('test')
})

usersRouter.post('/new', async (req,res) => {
    Logger.debug(util.inspect(req.body));
     /**Params:
     * Party: party: 'r'/'d'/'i'/'l'/'a'/'g' (republican, democrat, independent, libertarian, alliance, green)
     * State: state: 'ny/ca/fl' (state initials)
     * Charisma: 'charisma' (integer)
     * Connections: 'connections' (integer)
     * Endurance: 'endurance' (integer)
     * Communication: 'communication' (ineger)
     * (Charisma + Connections + Stamina + Communication MUST equal 4)
     * Politician Name: 'name' (string)
     * class: 'wealthy', 'middle', or 'poor' (string, lowercase)
     * Race: 'asian', "hispanic", "black", 'white', 'other'
     * Stance: "far_left", "center_left", "leans_left", "center", "leans_right", "center_right", "far_right"
     */
    if(!req.body.state || !req.body.party || !req.body.name || !req.body.class || !req.body.race || !req.body.stance || !req.body.bearer_token) {
        res.status(422);
        res.json({
            error: {
                code: 423, 
                message: 'Missing paramater',
            },
        });
        return;
    }
    let parties = ['r','d','i','l','a','g']
    let states = ['AK','AL','AR','AS','AZ','CA','CO','CT','DC','DE','FL','GA','GU','HI','IA','ID','IL','IN','KS','KY','LA','MA','MD','ME','MI','MN','MO','MS','MT','NC','ND','NE','NH','NJ','NM','NV','NY','OH','OK','OR','PA','PR','RI','SC','SD','TN','TX','UT','VA','VI','VT','WA','WI','WV','WY'];
    let classes = ['wealthy', 'middle', 'poor']
    let races = ['asian', "hispanic", "black", 'white', 'other']
    let stances = ["far_left", "center_left", "leans_left", "center", "leans_right", "center_right", "far_right"]
    if(!parties.contains(req.body.party.toLowerCase()) || !states.contains(req.body.state.toUpperCase()) || !classes.contains(req.body.class.toLowerCase()) || !races.contains(req.body.race.toLowerCase()) || !stances.contains(req.body.stance.toLowerCase())) {
        res.status(400);
        res.json({
            error: {
                code: 402, 
                message: 'Paramater incorrect',
            },
        });
        return;
    }
    let charisma 
    let connections
    let endurance
    let communication
    try {
        charisma = Number(req.body.charisma) || 0
        connections = Number(req.body.connections) || 0
        endurance = Number(req.body.endurance) || 0
        communication = Number(req.body.communication) || 0
    } catch(e) {
        res.status(400)
        res.json({
            error: {
                code: 400, 
                message: 'Paramater incorrect',
            },
        });
    }
    if(!charisma||!connections||!endurance||!communication) {
        res.status(400)
        res.json({
            error: {
                code: 400, 
                message: 'Paramater incorrect',
            },
        });
    }
    if(charisma+connections+endurance+communication !== 4) {
        res.status(400);
        res.json({
            error: {
                code: 401, 
                message: 'Paramater incorrect',
            },
        });
        return;
    }
    var respo = await db.table('users').get(req.body.bearer_token).run()
    console.log(respo)
    if(!respo || respo.length < 1) {
        res.status(300)
        return res.json({error: {
            code: 300,
            message: 'account does not exist'
        }})
    }
    let q = await db.table('user_stats').run();
    var dat = q.filter(x=>x.username == respo.username)
    console.log(dat.length)
    if(dat.length > 0) {
        res.status(409);
        res.json({error: {'code': 409, 'message': 'User already exists!'}});
        return;
    }
    let new_id = respo.id
    db.table('user_stats').insert({
        id: new_id,
        username: respo.username,
        money: 0,
        charisma: req.body.charisma,
        connections: req.body.connections,
        stamina: 100,
        communication: req.body.communication,
        s_influence: 0,
        n_influence: 0,
        party: req.body.party,
        state: req.body.state,
        politician_name: req.body.name,
        class: req.body.class,
        race: req.body.race,
        stance: req.body.stance,
        endurance: req.body.endurance
    }).run().then(rRes => {
        res.status(200);
        res.json({
            success: true,
            newid: new_id,
        });
    });
});

usersRouter.get('/new', (req,res) => {
    res.status(400);
    res.json({
        error: {
            code: 400, 
            message: 'Only POST is allowed.',
        },
    });
});
usersRouter.get('/stats', (req,res) => {
    Logger.debug(util.inspect(req.query));
    /**Params:
     * ID: 'id' (snowflake id of user)
     */
    if(!req.query.id) {
        res.status(422);
        res.json({
            error: {
                code: 422, 
                message: 'Missing paramater',
            },
        });
        return;
    }
    db.table('user_stats').get(req.query.id).run().then(data=>{
        if(!data) {
            res.status(404);
            res.json({
                error: {
                    code: 404, 
                    message: 'User does not exist!',
                },
            });
            return;
        } else {
            res.status(200);
            res.json(data);
        }
    });
});

usersRouter.get('/username-to-id', (req,res) => {
    /**Params:
     * Username: 'username' (users username to be transfered to id (all lowercase please))
     */
    if(!req.query.username) {
        res.status(422);
        res.json({
            error: {
                code: 422,
                message: 'Missing paramater',
            },
        });
        return;
    }
    db.table('user_stats').run()
    .then(a=>{
        var data = a.filter(x=>x.username == req.query.username)
        if(data.length < 1) {
            res.status(404);
            res.json({
                error: {
                    code: 404,
                    message: 'User does not exist!',
                },
            });
            return;
        } else {
            res.status(200);
            res.json({id: data[0].id});
        }
    });
});

module.exports = usersRouter;
