const express = require('express') 
const db = require('../db')
var r = new express.Router()
const rq = require('request-promise')
const flake = require('simpleflake')
const secret = require('./authSecret.json').secret
const appid = require('./authSecret.json').id
const crypto = require('crypto')
const baseuri = require('./baseuri')
// begin stuff
//const baseuri = 'http://localhost:2800'
// end stuff idk fam
// begin magic stackoverflow code
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
// end ctrl+c ctrl+v
r.get('/discord', async (req, res) => {
    if (req.session.discord) return res.redirect('/auth')
    if (!req.query.code) return res.redirect(`https://discordapp.com/api/oauth2/authorize?client_id=${appid}&redirect_uri=https%3A%2F%2Ffanderov.jt3ch.net%2Fauth%2Fdiscord&response_type=code&scope=email%20identify`)

    console.log(req.query.code)
    var data = {
        client_id: appid,
        client_secret: secret,
        grant_type: 'authorization_code',
        code: req.query.code,
        redirect_uri: `${baseuri}/auth/discord`,
        scope: 'identify email connections'
    }
    console.log(data)
    var response = await rq.post("https://discordapp.com/api/v6/oauth2/token", {
        form: data
    }).catch(e => {
        console.log(e)
        res.json({error: "invalid_token"})
    })
    if (response == undefined) return 0;
    var response = JSON.parse(response)
    
    console.log(response)
    var user = await rq.get("https://discordapp.com/api/v6/users/@me", {
        headers: {
            "Authorization": "Bearer " + response.access_token
        }
    })
    var user = JSON.parse(user)
    req.session.email = user.email
    req.session.discord = true
    req.session.discord_id = user.id
    req.session.username = user.username
    var bearer = crypto.createHash("sha256").update(flake(new Date())).digest('base64')
    db.table("users").filter({
        discord: {id: user.id}
    }).run((e, result) => {
        if (e) return console.log(e)
        // console.log(result)
        if (isEmpty(result)) {
            console.log("adding users")
            db.table("users").insert({
                email: user.email,
                username: user.username,
                id: flake(new Date()).toString("base10"),
                discord: {
                    id: user.id
                },
                bearer: bearer
            }).run(console.log)
            res.cookie('bearer', bearer).json({status: "post_oauth_complete", next: "redirect_to_endpoint"})
        } else {
            console.log('not adding users')
            res.cookie('bearer', result[0].bearer).json({status: "post_oauth_complete", next: "redirect_to_endpoint"})
        }
    })

    //res.cookie("discord", "yes").cookie("discord_id", user.id).redirect('/auth')
    
})

r.post('/basic', (req, res) => {
    console.log(req.body)
    var username = req.body.username
    // var password = crypto.createHash('sha256').update(req.body.password).update(req.session.basic_token).digest('hex');
    db.table('users').filter({
        username: username
    }).run(async (e, result) => {

        if (e) return console.log(e)
        if (result.length == 0 ) return res.json({error: "account_no_exist"})
        var respo = await db.table('user_stats').get(result[0].id).run()// put 
        if(!respo || respo.length < 1) return res.json({error: 'no_stats'}) // this code on line 94
        var user = result[0]
        if (user.basic_auth.password != crypto.createHash('sha256').update(req.body.password).update(user.basic_auth.token).digest('hex')) return res.json({error: "account_no_exist"})

        res.json({status: 200, step: "post_auth_complete", bearer: user.bearer})
    })
})
r.post('/basic/signup', (req, res) => {
    console.log(req.body)
    var username = req.body.username
    var email = req.body.email
    var id = flake(new Date()).toString("base10")
    var token = crypto.createHash('sha256').update(flake(new Date())).digest('base64')
    var password = crypto.createHash('sha256').update(req.body.password).update(token).digest("hex")
    var bearer = crypto.createHash("sha256").update(flake(new Date())).digest('base64')
    db.table('users').filter({
        username: username
    }).run((e, result) => {
        if (e) return console.log(e)
        if (result.length == 1) return res.json({error: "username_exists"})
        if (result.length == 0) console.log("new signup")
        db.table('users').insert({
            id: id,
            username: username,
            email: email,
            basic_auth: {
                password: password,
                token: token
            },
            bearer: bearer
        }).run()
        req.session.basic_token = token
        res.cookie('bearer', bearer).redirect('/static/loginpage#login')
    })
})

r.get('/basic', (req, res) => {
    res.json({error: "invalid_request"})
    
})
r.get('/logout', (req, res) => {
    req.session.destroy()
    res.clearCookie("bearer").redirect('/static/loginpage')
})
module.exports = r