class Request {
    constructor(req) {
        this.original = req;
        this.query = req.query;
        this.params = req.params;
        this.body = req.body;
    }
}

module.exports = Request;