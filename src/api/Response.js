class Response {
    constructor(res) {
        this.original = res;
    }

    json(success, data, error, errCode) {
        if (success) return this.original.statusCode(200).json({
            success: true,
            data,
            error: null,
        });
        else return this.original.statusCode(errCode).json({
            success: false,
            data: null,
            error:
            {
                message: error,
                code: errCode,
            }
        });
    }
}

module.exports = Response;