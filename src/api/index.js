const express = require('express');
const rethinkdbdash = require('rethinkdbdash');
const basicAuth = require('express-basic-auth');
const bodyParser = require('body-parser');

const Logger = require('./Logger');
const Request = require('./Request');
const Response = require('./Response');

class API {
    constructor(opts) {
		Logger.info('Initializing RethinkDB');
		this.r = rethinkdbdash(opts.rethinkdb);
		Logger.info('Done initializing RethinkDB');

		this.app = express();
		this.port = process.env.PORT || opts.port || 3000

		Logger.info('Initializing body-parser');
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({
			extended: true,
		}));
		Logger.info('Done initializing body-parser');
    }

    auth(type){
        switch (type.toLowerCase()) {
			case 'basic':
				Logger.info('Initializing Basic Auth');

                this.app.use(basicAuth({
                    authorizer: async (username, password) => {
						const login = await this.r.table('logins').get({
							username,
							password,
						});

						return !!login;
					},
                }));
				Logger.info('Done initializing Basic Auth');
				break;
			default:
				Logger.error(`${type.toUpperCase()} is not a compatible auth type!`)
				break;
        }
	}

	get(route, cb) {
		this.app.get(route, (req, res) => {
			const newReq = Request(req);
			const newRes = new Response(res);
			cb(newReq, newRes, this.r, Logger);
		})
	}
	
	post(route, cb) {
		this.app.post(route, (req, res) => {
			const newReq = Request(req);
			const newRes = new Response(res);
			cb(newReq, newRes, this.r, Logger);
		})
	}

	launch() {
		this.app.listen(this.port);
		Logger.info(`Starting app on port ${this.port}`);
	}
}

module.exports = API;
