/*eslint-disable unknown-require, no-unused-vars, no-unused-params*/
var app = require('express')();
var http = require('http').Server(app);
const bodyParser = require('body-parser');
var ioserver = require('./classes/ChatIOServer');
const chatserver = new ioserver(require('socket.io')(http));
const Logger = require('./Logger');
const path = require('path');
const User = require('./classes/Author');
const Command = require('./classes/Command');
const Message = require('./classes/Message');
var cookieParser = require('cookie-parser');
const logger = require('./classes/ChatLogger');
const chalk = require('chalk')
app.use(cookieParser());
const config = {
    port: 2800,
};
const session = require('express-session')
app.use(session({
    secret: "LitFamSquadlit13376969420"
}))
app.use(bodyParser())
app.use('/auth', require('./routes/auth'))
app.use('/users', require('./routes/users'));
app.use('/chat', require("./routes/chatUsers"));
app.get('/static/ee.js', (req, res) => {
    res.sendFile(path.join(__dirname, "classes", "EventEmitter.min.js"));
});
app.get('/static/client.js', (req, res) => {
    res.sendFile(path.join(__dirname, "classes", "ChatClient.js"));
});
app.get('/static/testpage', (req, res) => {
    res.sendFile(path.join(__dirname, "html", "index.html"));
});
app.get('/static/loginpage', (req, res) => {
    res.sendFile(path.join(__dirname, "html", "login.html"));
});
app.get('/static/statspage', (req, res) => {
    res.sendFile(path.join(__dirname, "html", "stats.html"));
});
app.get('/', (req,res) => {
    res.status(200);
    res.json({
        hello: 'world'
    });
});
app.post('/auth/disconnect/:bearer', async (req, res) => {
    console.log(req.params)
    var bearer = req.params.bearer
    var user = await new User().bearer_auth(bearer)
    new Message("[DISCONNECT] " + user.name, "server", "any", true).send(chatserver.io, true)
    logger.custom(chalk`{bold.cyan [USER-DISCONNECT]} {bold.grey ${logger.user(user)}}`);
})
chatserver.register_command(new Command({
    command: "channel",
    context: "globalChannel",
    execute: function(user, channel, message, context, io) {
        // pid
        // console.log(user, channel, message, context)
        // console.log("command test run")
        // console.log(nio)
        new Message(`You are in ${channel}.`, "server", "global", true).send(io, true);
    }
    
}));
chatserver.register_command(new Command({
    command: "level",
    context: "globalChannel",
    execute: function(user, channel, message, context, io) {
        // console.log(user, channel, message, context)
        // console.log("command test run")
        // console.log(nio)
        new Message(`You have permission level <b>${user.permissionlevel}</b>`, "server", "global", true).send(io, true);
    }
    
}));
chatserver.register_command(new Command({
    command: "dab",
    context: "globalChannel",
    execute: function(user, channel, message, context, io) {
        // console.log(user, channel, message, context)
        // console.log("command test run")
        // console.log(nio)
        console.log(new Message(`<i>dabs</i>`, user.json(), channel))
        new Message(`<i>dabs</i>`, user.json(), channel).send(chatserver.io);
    }
    
}));

http.listen(config.port, () => {
    Logger.info(`Starting app on port ${config.port}`);
});
